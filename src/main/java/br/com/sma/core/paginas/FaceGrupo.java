package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.properties.Data;
import org.openqa.selenium.By;

public class FaceGrupo extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------

    //--------------------------------- OTHER METHODS ------------------------------------
    public void preencherPublicacao() {
        log.debug("preencherPublicacao");
        if(Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//textarea[@data-testid='status-attachment-mentions-input']"))))
            preencherCampo(By.xpath("//textarea[@data-testid='status-attachment-mentions-input']"), Data.get("nomeProduto"));

        else if(Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//input[@placeholder='O que você está vendendo?']")))){
            log.info("Grupo de vendas... preenchendo preço qualquer...");
            clicar(By.xpath("//input[@placeholder='O que você está vendendo?']"));
            Wait.setWait(2000);
            preencherCampo(By.xpath("//input[@placeholder='O que você está vendendo?']"), Data.get("nomeProduto"));
            preencherCampo(By.xpath("//input[@placeholder='Preço']"), "1500");
            preencherCampo(By.xpath("//div[@data-testid='react-composer-root']/div/div/div/div[4]/div/div/div[@role='presentation']"), Data.get("descricaoProduto"));
        }
    }

    public void clicarBotaoPublicar() {
        log.debug("clicarBotaoPublicar");
        if(Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//button[@data-testid='react-composer-post-button']"))))
            clicar(By.xpath("//button[@data-testid='react-composer-post-button']"));

        Wait.setWait(5000);

        if(Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//ul[contains(@class,'uiList')]/li/div")))){
            int itens = checkElements(By.xpath("//ul[contains(@class,'uiList')]/li/div")).size();

            for(int i = 2; i <= itens; i++)
                clicar(By.xpath("(//ul[contains(@class,'uiList')]/li/div/div[2]/div)["+i+"]"));
        }

        Wait.setWait(By.xpath("//button[@data-testid='react-composer-post-button']"));
        clicar(By.xpath("//button[@data-testid='react-composer-post-button']"));
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}