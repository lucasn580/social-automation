package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import br.com.sma.core.utilitario.base.Wait;
import org.openqa.selenium.By;

public class FaceInicial extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------

    //--------------------------------- OTHER METHODS ------------------------------------
    public void acessarGrupos() {
        log.debug("acessarGrupos");
        if(existsElement(By.xpath("//div[@id='appsNav']/ul/li/div[text()='Grupos']")))
            clicar(By.xpath("//div[@id='appsNav']/ul/li/div[text()='Grupos']"));

        else if(existsElement(By.xpath("//div[@id='appsNav']/ul/li/a/div[text()='Grupos']")))
            clicar(By.xpath("//div[@id='appsNav']/ul/li/a/div[text()='Grupos']"));
    }

    public void clicarGrupo(String grupo) {
        log.debug("clicarGrupo");
        if(Wait.setWait(() -> Wait.visibilityOfElementLocated(By.xpath("//ul/li/ul/li/div/div/div/div/div/a[contains(text(),'" + grupo + "')]"))))
            clicar(By.xpath("//ul/li/ul/li/div/div/div/div/div/a[contains(text(),'" + grupo + "')]"));
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}