package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class InstaPerfil extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------

    //--------------------------------- OTHER METHODS ------------------------------------
    public void visitarPerfil(String user){
        log.debug("visitarPerfil " + user);
        acessarUrl(Data.get("url.inicial") + user);
    }

    public void deixarDeSeguir(){
        log.debug("deixarDeSeguir");
        if(existsElement(By.xpath("//button[text()='Seguindo']"))){
            clicar(By.xpath("//button[text()='Seguindo']"));
            clicar(By.xpath("//button[text()='Deixar de seguir']"));
            Wait.setWait();
        }
    }

    public void abrirSeguidores(){
        log.debug("abrirSeguidores");
        clicar(By.cssSelector("#react-root > section > main > div > header > section > ul > li:nth-child(2) > a > span"));
    }

    public void fecharSeguidores(){
        log.debug("fecharSeguidores");
        clicar(By.cssSelector("body > div> div > div > div > div:nth-child(3) > button"));
    }

    public int contarSeguidores(){
        log.debug("contarSeguidores");
        return Integer.valueOf(getText(By.cssSelector("#react-root > section > main > div > header > section > ul > li:nth-child(2) > a > span")));
    }

    public List<String> capturarSeguidores(int cont){
        log.debug("capturarSeguidores");

        List<String> seguidores = new ArrayList<>();

        if(Wait.setWait(5000, () -> Wait.visibilityOfElementLocated(By.cssSelector("body > div > div > div > div > ul > div")))){

            for(int i = 1; i <= cont; i++){

                if(existsElement(By.xpath("(//a[contains(@class,'notranslate')]//ancestor::li)[" + i + "]"))){
                    scrollElementIntoView(By.xpath("(//a[contains(@class,'notranslate')]//ancestor::li)[" + i + "]"));
                    seguidores.add(getText(By.xpath("(//a[contains(@class,'notranslate')])[" + i + "]")));
                } else
                    log.warn(TratarString.colored("Seguidor número " + i + " não aparece na lista", TratarString.Cores.VERMELHO, null, null));
            }
        }
        log.info(TratarString.colored("Número atual de seguidores: " + seguidores.size(), TratarString.Cores.VERDE, null, TratarString.Formatacao.NEGRITO));

        return seguidores;
    }

    private boolean validarInteracao(){
        log.debug("Validando se é possível interagir com perfil ex: curtir fotos");
        Wait.setWait();

        int posts = Integer.parseInt(getText(By.cssSelector("#react-root > section > main > div > header > section > ul > li > span > span")).replace(".", ""));
        if(posts == 0){
            log.warn("Este usuário não possui publicações");
            return false;
        }
        if(existsElement(By.cssSelector("#react-root > section > main > div > div > article > div > div > h2"))){
            log.warn("Esta conta é privada");
            return false;
        }
        return true;
    }

    protected boolean clicarPublicacao(int linha, int coluna){
        log.info("clicarPublicacao linha: " + linha + " coluna: " + coluna);

        if(validarInteracao()){
            if(Wait.setWait(5000, () -> Wait.visibilityOfElementLocated(By.cssSelector("#react-root > section > main > div > div > article > div > div")))){
                clicar(By.cssSelector("#react-root > section > main > div > div > article > div > div > div:nth-child(" + linha + ") > div:nth-child(" + coluna + ")"));
                return true;
            }
            log.debug("Publicação não clicada");
        }
        deletarCookie("mid");
        return false;
    }

    //--------------------------------- END OF OTHER METHODS ------------------------------------
}