package br.com.sma.core.paginas;

import br.com.sma.core.utilitario.base.BaseForPages;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import org.openqa.selenium.By;

public class InstaInicial extends BaseForPages {
    //--------------------------------- FIELDS ------------------------------------

    //--------------------------------- OTHER METHODS ------------------------------------
    public void verificarPopUpNotificacoes() {
        log.debug("verificarPopUpNotificacoes");
        if(existsElement(By.className("piCib")))
            clicar(By.xpath("//button[text()='Agora não']"));
    }

    public void acessarMeuPerfil() {
        log.debug("acessarMeuPerfil");
        clicar(By.xpath("//a[@href=\"/" + Data.get("User") + "/\"]"));
    }

    public void realizarBuscaTag(String tag) {
        log.info("Tag buscada: " + TratarString.colored(tag, TratarString.Cores.VERDE, null, TratarString.Formatacao.NEGRITO));

//        preencherCampoAutoComplete(By.cssSelector("#react-root > section > nav > div > div > div > div:nth-child(2)"), "#" + valor);
//        Wait.setWait(5000);

        acessarUrl("https://www.instagram.com/explore/tags/" + tag);
        Data.set("urlBuscada", Data.get("urlAtual"));
    }

    public void realizarBuscaLocal(String local) {
        log.info("Local buscado: " + TratarString.colored(local, TratarString.Cores.VERDE, null, TratarString.Formatacao.NEGRITO));

        acessarUrl("https://www.instagram.com/explore/locations/" + local);
        Data.set("urlBuscada", Data.get("urlAtual"));
    }
    //--------------------------------- END OF OTHER METHODS ------------------------------------
}