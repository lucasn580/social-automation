package br.com.sma.core.utilitario.experimental.junit;

import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.tratamentos.TratarNumero;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import org.junit.runner.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class ListenerMessages {
    // ------------------------------ FIELDS ------------------------------
    private final Logger log = LoggerFactory.getLogger(ListenerMessages.class.getSimpleName());
    private static Result result;

    // ------------------------------ OTHERS METHODS ------------------------------
    protected void Contador(int passedTests) {
        if (Data.get("QuantidadeDeCasoDeTesteQuePassou") == null)
            Data.set("QuantidadeDeCasoDeTesteQuePassou", "0");

        else {
            Data.set("QuantidadeDeCasoDeTesteQuePassou", String.valueOf(passedTests));
        }
    }

    // ------------------------------ MESSAGE METHODS ------------------------------
    protected void logMessage() {
        String mensagem;
        String mensagemFluxo = "\n\n" +
                "*****************************************************************************************************************************\n" +
                "\n" +
                "                                  Iniciando fluxo: " + TratarString.colored(Data.get("nomeFluxo"), TratarString.Cores.VERMELHO, null, null) + "\n" +
                "\n" +
                "*****************************************************************************************************************************";

        String mensagemCT = "\n\n..............................................................................................................................\n" +
                "\n" +
                "                                  Passaram ate agora: " + TratarString.colored(Data.get("QuantidadeDeCasoDeTesteQuePassou"), TratarString.Cores.VERMELHO, null, null) +
                "\n" +
                "                                  Fluxo: " + TratarString.colored(Data.get("nomeFluxo"), TratarString.Cores.VERMELHO, null, null) +
                "\n" +
                "                                  CT: " + TratarString.colored(Data.get("classDescription"), TratarString.Cores.VERMELHO, null, null) + "\n" +
                "\n" +
                "..............................................................................................................................\n";

        if (!Objects.equals(Data.get("nomeFluxo"), Data.get("nomeFluxoAtual"))) {
            Data.set("nomeFluxoAtual", Data.get("nomeFluxo"));
            mensagem = mensagemFluxo + mensagemCT;

        } else
            mensagem = mensagemCT;

        log.info(mensagem);
    }

    protected void logErrorMenssage() {
        log.error("\n\n..............................................................................................................................\n" +
                "\n" +
                "                                  " + TratarString.colored("Falha no Teste...", TratarString.Cores.VERMELHO, null, null) + "\n" +
                "\n" +
                "                                  CT: " + TratarString.colored(Data.get("classDescription"), TratarString.Cores.VERMELHO, null, null) + "\n" +
                "\n" +
                "..............................................................................................................................\n");
    }

    public void showResults(int falhados) {
        String message;
        message = "\n\n..............................................................................................................................\n" +
                "\n" +
                TratarString.colored("                                 CTs Executados: " + result.getRunCount(),TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO)  +"\n"+
                "\n" +
                TratarString.colored("                                 CTs Aprovados: " + Data.get("QuantidadeDeCasoDeTesteQuePassou"), TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO)  +"\n"+
                "\n" +
                TratarString.colored("                                 CTs Falhados: " + falhados, TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO)  +"\n"+
                "\n" +
                        (result.getIgnoreCount()>0 ?
                                TratarString.colored("                                 CTs Ignorados: " + result.getIgnoreCount(), TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO) +"\n" + "\n" : "") +

                TratarString.colored("                                 Run Time: " + TratarNumero.formatarTempo(result.getRunTime()), TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO) +"\n" +
                "\n" +
                "..............................................................................................................................\n" +
                "\n" +
                TratarString.colored("                                 " + (result.wasSuccessful() ? "BUILD SUCCESS" : "BUILD FAILED"), TratarString.Cores.AZUL, null, TratarString.Formatacao.NEGRITO) +"\n" +
                "\n" +
                "..............................................................................................................................\n";
        log.info(message);
    }

    public void setResult(Result result){
        this.result = result;
    }

    public Result getResult(){
        return result;
    }

    // ------------------------------ END OF OTHERS METHODS ------------------------------
}
