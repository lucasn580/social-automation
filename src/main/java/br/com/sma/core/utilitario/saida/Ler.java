package br.com.sma.core.utilitario.saida;

import br.com.sma.core.utilitario.base.Util;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

public class Ler {
    // ------------------------------ FIELDS ------------------------------
    private static final Logger log = LoggerFactory.getLogger(Ler.class.getSimpleName());
    private static final String QUERY_FILE = "query/";

    // -------------------------- OTHER METHODS --------------------------
    public static String ler(String completePath) {
        return lerArquivo(Util.pegarResourcePath(completePath));
    }

    /**
     * Executa a query.
     *
     * @param nomeQuery      Nome da query.
     * @param listaParamento Parametros para passar para a query.
     * @return Resultado da query.
     */
    public static String pegarQuery(String nomeQuery, List<String> listaParamento) {
        log.debug("Pegando o resultado da Query " + nomeQuery);

        int cont = 0;
        String query = lerArquivo(QUERY_FILE + nomeQuery);

        if (query != null)
            if (listaParamento != null)
                while (cont < listaParamento.size())
                    query = query.replaceAll("Parametro" + ++cont, listaParamento.get(cont - 1));

        return query;
    }

    public static String pegarQuery(String nomeQuery, String... paramentos) {
        log.debug("Pegando o resultado da Query " + nomeQuery);

        int cont = 0;
        String query = lerArquivo(QUERY_FILE + nomeQuery);

        if (query != null)
            if (paramentos != null)
                for (String parametro : paramentos)
                    query = query.replaceAll("Parametro" + ++cont, parametro);

        return query;
    }

    @Nullable
    private static String lerArquivo(String completePath) {
        log.debug("Lê o arquivo passado");

        String line;
        StringBuilder value = new StringBuilder();
        File file = new File(completePath);

        try {
            if (file.exists()) {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                while ((line = bufferedReader.readLine()) != null)
                    value.append(line).append("\n");

                bufferedReader.close();

            } else {
                InputStream resource = Ler.class.getClassLoader().getResourceAsStream(String.valueOf(file));

                if (resource != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource));

                    while ((line = bufferedReader.readLine()) != null)
                        value.append(line).append("\n");

                    bufferedReader.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(value);
    }
}