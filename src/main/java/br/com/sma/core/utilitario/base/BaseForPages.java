package br.com.sma.core.utilitario.base;

import br.com.sma.core.auxiliares.helper.AssertPage;
import br.com.sma.core.utilitario.properties.Data;

public class BaseForPages extends Page {
    // ------------------------------ FIELDS ------------------------------

    // -------------------------- OTHER METHODS --------------------------
    protected BaseForPages() {

        if (Data.get("IDWF") != null) {
            if (!Data.get("IDWF").equals("922802") && Data.get("IDWF").equals("1225187")) {
                AssertPage assertPage = new AssertPage();
                assertPage.assertPage();
            }
        }
    }

    // -------------------------- END OF OTHER METHODS --------------------------
}