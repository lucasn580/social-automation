package br.com.sma.core.utilitario.base;

import br.com.sma.core.auxiliares.helper.AssertPage;
import br.com.sma.core.utilitario.driver.WebDriverRecicle;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.tratamentos.TratarNumero;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static br.com.sma.core.utilitario.base.Wait.*;
import static br.com.sma.core.utilitario.driver.WebDriverFactory.webDriverInstance;
import static org.openqa.selenium.support.ui.ExpectedConditions.alertIsPresent;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class Page {
    // ------------------------------ FIELDS ------------------------------
    protected static final Logger log = LoggerFactory.getLogger(Page.class.getSimpleName());

    // -------------------------- OTHER METHODS --------------------------

    /**
     * Pega uma lista de elemento apartir do localizador
     *
     * @param locator localizador do WebElement
     * @return retorna uma lista de elemento referenciado pelo localizador
     */
    private static List<WebElement> getElements(By locator) {
        log.debug("retorna uma lista de elemento referenciado pelo localizador...");
        return webDriverInstance.findElements(locator);
    }

    /**
     * Retorna o valor da URL que esta navegendo no momento;
     *
     * @return String da URL;
     */
    public static String pegarURLAtual() {
        log.debug("Pegando URL Atual...");
        return webDriverInstance.getCurrentUrl();
    }

    /**
     * Checks if a given element exists in page
     *
     * @param locator The element selector by *name*
     * @return true if element exists or false otherwise
     */
    public static boolean existsElement(By locator) {
        log.debug("Verifica se existe o elemento passado por parametro na tela...");
        return getElements(locator).size() != 0;
    }

    /**
     * Esconde um elemento
     *
     * @param locator localizador do elemento By cssDelector
     */
    public static void hideElement(By locator) {
        log.debug("Escondendo o elemento...");
        jQuery("(\"" + locator.toString().replaceAll("By.cssSelector: ", "") + "\").hide()");
    }

    /**
     * Faz highlight no elemento
     *
     * @param locator localizador do elemento
     */
    protected static void highlightElement(By locator) {
        log.debug("Fazendo highlight no elemento...");
        try {
            js("arguments[0].style.border='3px solid " + Data.get("color") + "'", locator);
        } catch (Exception e) {
            log.debug("Não pode aplicar o highligth", e);
        }
    }

    /**
     * Executa comando do tipo JQuery
     *
     * @param script  Comando JQuery
     * @param objects Onjeto para complemntar o comando
     * @return Se tiver resultado, Retorna um objeto com as informaçoes que o script retorna
     */
    protected static Object jQuery(String script, Object... objects) {
        String prefix = "return " + getJQueryPrefix();

        try {
            return (objects != null) ?
                    ((JavascriptExecutor) webDriverInstance).executeScript(prefix + script, objects) :
                    ((JavascriptExecutor) webDriverInstance).executeScript(prefix + script);

        } catch (Exception e) {
            log.warn("Não foi possível executar jQuery. Tentando de novo... "/*, e*/);
            log.debug("JS:" + script);

            return (objects != null) ?
                    ((JavascriptExecutor) webDriverInstance).executeScript(prefix + script, objects) :
                    ((JavascriptExecutor) webDriverInstance).executeScript(prefix + script);
        }
    }

    /**
     * Pega o webElement paratir do localizador
     *
     * @param locator localizador do elemento
     * @return retorna o WebElement refereniado pelo localizador
     */
    private static WebElement getElement(By locator) {
        log.debug("Pega o webElement paratir do localizador...");
        highlightElement(locator);
        return webDriverInstance.findElement(locator);
    }

    /**
     * Executa comandos do tipo JavaScript
     *
     * @param script  Comando que deve ser executado
     * @param objects Objeto para complementar o comando
     * @return Se tiver resultado, Retorna um objeto com as informaçoes que o script retorna
     */
    private static Object js(String script, Object... objects) {
        return ((JavascriptExecutor) webDriverInstance).executeScript(script, objects);
    }

    /**
     * Pega o prefixo para executar um JQuery
     *
     * @return retorna $j se estiver definido o tipo se não retorna somente $
     */
    @NotNull
    private static String getJQueryPrefix() {
        return (Objects.equals(js("return typeof $j"), "undefined")) ?
                "$" : "$j";
    }

    /**
     * Tirar um Print em formato png da tela.
     * Lembre-se que arquivos de mesmo nome serão sobrescritos
     *
     * @param nomeArquivo Nome do print que sera feito;
     */
    public static void capturarScreenshot(String nomeArquivo) {
        log.debug("Pegando Print da tela...");
        String nomeMetodo = "capturarScreenshot";

        setWait();

        if (!("".equals(nomeArquivo) || nomeArquivo == null)) {

            nomeArquivo = nomeArquivo.replaceAll(":", "_");
            nomeArquivo = nomeArquivo.replaceAll(" ", "");

            try {
                if (WebDriverRecicle.verificarWebDriverAberto()) {
                    File diretorio = new File("target/screenshots");
                    if (!diretorio.exists()) {
                        log.debug("Nao existe! \nCriando");
                        diretorio.mkdirs();
                    }

                    log.debug(String.format("Diretorio saida prints: %s", diretorio.getCanonicalPath()));

                    if (WebDriverRecicle.verificarWebDriverAberto()) {
                        File arquivo = ((TakesScreenshot) webDriverInstance).getScreenshotAs(OutputType.FILE);
                        File destFile = new File(diretorio + "/" + nomeArquivo + ".png");
                        log.debug(String.format("Gravando screenshot: %s", destFile.getCanonicalPath()));
                        FileUtils.copyFile(arquivo, destFile);
                    }
                }
            } catch (IOException e) {
                log.warn(String.valueOf("\n\n**************Erro!!**************\n\n" + e + "\n\n" + nomeMetodo + " --> " + nomeArquivo));
            }
        }
    }

    /**
     * Acessa a URL indicada
     *
     * @param url URL para acessar
     */
    public void acessarUrl(String url) {
        log.debug("acessar a url");
        webDriverInstance.get(url);
    }

    /**
     * Atualiza a janela
     */
    public void atualizarTela() {
        log.debug("Refresh da tela...");
        webDriverInstance.navigate().refresh();
    }

    /**
     * Clica no botao passado no localizador
     *
     * @param locator     Localizador By do elemento.
     * @param eachElement Qual o numero do elemento que vc quer clicar da lista.
     */
    public void clicar(By locator, Integer eachElement) {
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        log.debug("Clicando no botão... " + locator.toString());
        setWait(() -> Wait.elementToBeClickable(locator));

        clicar(locator, checkElements(locator).get(eachElement));
    }

    /**
     * Clica no botao passado no localizador
     *
     * @param locator Localizador By do elemento.
     */
    public void clicar(By locator) {
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        log.debug("Clicando no botão... " + locator.toString());
        setWait(() -> Wait.elementToBeClickable(locator));

        while (existsElement(By.xpath("//svg"))){
            Wait.setWait(1000);
            log.info("Aguardando carregamento...");
        }

        clicar(locator, checkElement(locator));
    }

    /**
     * Realiza a rolagem da tela
     *
     */
    public void scrollToBottomPage(){
        Actions actions = new Actions(webDriverInstance);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
        actions.keyDown(Keys.CONTROL).release().perform();
    }

    /**
     * Realiza a rolagem da tela com Javascript
     *
     */
    public void scrollToElementJS(By locator) {
        try{
            int Y = (getElement(locator).getLocation().getY() - 200);
            js("javascript:window.scrollTo(0," + Y + ");");
        } catch (Exception e ){
            log.info("Elemento não encontrado via Javascript");
        }

    }

    /**
     * Realiza a rolagem da tela com Javascript
     *
     */
    public void scrollElementIntoView(By toLocator) {
        try{
            Actions clickAction = new Actions(webDriverInstance);
            WebElement element = webDriverInstance.findElement(toLocator);
            clickAction.moveToElement(element).build().perform();
        } catch (Exception e ){
            log.info("Elemento não encontrado via Javascript");
        }

    }

    /**
     * Realiza a rolagem customizada com Javascript
     *
     */
    public void scrollCustomJS(int value){
        try{
            js("scroll(0, "+value+");");
        } catch (Exception e ){
            log.debug("Rolagem de página em pixels: "+value);
        }
    }

    /**
     * Retorna se um elemento possui scroll
     *
     */
    public Boolean isScrollable(By locator){
        String JS_ELEMENT_IS_SCROLLABLE =
                "return arguments[0].scrollHeight > arguments[0].offsetHeight;";

        WebElement container = webDriverInstance.findElement(locator);
        return (Boolean)js(JS_ELEMENT_IS_SCROLLABLE, container);
    }


    /**
     * Preenche o alerta com o texto passado por paramento
     *
     * @param texto texto a ser preenchido
     */
    public void preencherPopUpAlert(String texto) {
        webDriverInstance.switchTo().alert().sendKeys(texto);
    }

    /**
     * Volta para o Frame Default
     */
    public void voltarFrameDefault() {
        log.debug("Volta o foco do driver para a paginas default...");
        webDriverInstance.switchTo().defaultContent();
    }

    /**
     * Fecha a janela Atual
     */
    public void fecharJanela() {
        log.debug("Fechando a janela atual...");
        webDriverInstance.close();
    }

    /**
     * Manda o comando 'Back' ao browser.
     */
    public void voltarBrowser() {
        log.debug("Envia o comando Back para o navegador...");
        webDriverInstance.navigate().back();
    }

    /**
     * Troca para a Janela desejada;
     *
     * @param url Pagina que queira trocar.
     */
    public void trocarJanela(String url) {
        log.debug("Pesquisa Janela: " + url);
        setWait();
        try {
            for (String janela : webDriverInstance.getWindowHandles()) {
                webDriverInstance.switchTo().window(janela);

                if (webDriverInstance.getCurrentUrl().contains(url))
                    break;
            }
        } catch (Exception e) {
            log.debug("Não foi possivel trocar janela", e);
        }
        setWait();
    }

    /**
     * Troca para o proximo Frame ou IFrame.
     */
    public void trocarFrame() {
        log.debug("Realiza a troca de frame...");
        setWait("\n" +
                        "Nao foi possivel achar o elemento 'By.tagName(\"iframe\")'" +
                        "\n Tentando trocar o Frame mesmo assim...",
                () -> frameToBeAvailableAndSwitchToIt(By.tagName("iframe")));
    }

    /**
     * Troca para a Janela desejada;
     *
     * @param url Pagina que queira trocar.
     */
    public void capturarPageSource(String url) {
        log.debug("Pesquisa Aba: " + url);
        setWait();
        try {
            webDriverInstance.navigate().to(url);
            log.info(webDriverInstance.getPageSource());
        } catch (Exception e) {
            log.debug("Não foi possivel trocar aba", e);
        }
        setWait();
    }

    /**
     * @param quantidade Quantidade de checkbox a serem selecionados
     *                   CUIDADO!! Se tiver checkbox fora da table, verificar seleção, pois está verificando apenas os que estão sendo exibidos.
     */
    public void selecionarCheckbox(Integer quantidade) {
        log.debug("Seleciona X checkbox e filtra por página...");
        Integer conut = 0;
        By locator = By.cssSelector("input[type='checkbox']");

        setWait(() -> Wait.elementToBeClickable(locator));

        List<WebElement> tableRows = checkElements(locator);

        for (WebElement element : tableRows) {
            if (element.isDisplayed()) {
                if (!element.isSelected())
                    element.click();

                conut++;
            }

            if (conut >= quantidade)
                break;
        }
    }


    /**
     * @param locator locator do elemento
     */
    public void selecionarCheckbox(By locator) {
        log.debug("Seleciona X checkbox e filtra por página...");
        setWait(() -> Wait.elementToBeClickable(locator));

        List<WebElement> tableRows = checkElements(locator);

        for (WebElement element : tableRows) {
            if (element.isDisplayed()) {
                if (!element.isSelected())
                    element.click();
            }
        }
    }

    /**
     * Seleciona uma opção do combo apartir de um pedaço do texto
     *
     * @param locator localizador do elemento.
     * @param texto   pedaço do texto para ser usado.
     */
    public void selecionarComboPorTexto(By locator, String texto) {
        log.debug("Selecionando um combo apartir de um pedaço de texto...");

        setWait(() -> visibilityOfElementLocated(locator));

        if (existsElement(locator)) {

            WebElement element = checkElement(locator);

            if (element == null)
                element = getElement(locator);

            Select select = new Select(element);

            for (WebElement webElement : select.getOptions()) {
                if (TratarString.tudoL(getText(webElement)).contains(TratarString.tudoL(texto))) {
                    select.selectByVisibleText(webElement.getText());
                    break;
                }
            }
        }
    }

    /**
     * Retorna a opção selecionada no combo
     *
     * @param locator localizador do elemento.
     */
    public String pegarSelecaoDoCombo(By locator) {
        log.debug("Resgatando a opção selecionada no combo...");

        setWait(() -> visibilityOfElementLocated(locator));

        if (existsElement(locator)) {

            WebElement element = checkElement(locator);

            if (element == null)
                element = getElement(locator);

            Select select = new Select(element);
            return select.getFirstSelectedOption().getText();
        }
        return "";
    }

    /**
     * Seleciona o combo passando o nome do combo como ira chamar ele 'por' (ex: id, name...);
     * O tipo de do valor 'tipo' (ex: index, text, value) e o valor a chamar 'texto' ou 'index';
     *
     * @param locator Localizador do elemento
     * @param tipo    'index', 'text', 'value';
     * @param texto   Se tiver, Texto que ira ser selecionado;
     * @param index   Se tiver, Index que ira selecionar;
     */
    public void selecionarCombo(By locator, String tipo, String texto, Integer index) {
        log.debug("selecionando um Combo...");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        setWait(() -> visibilityOfElementLocated(locator));

        if (existsElement(locator)) {

            WebElement element = checkElement(locator);

            if (element == null)
                element = getElement(locator);

            Select combo = new Select(element);
            Integer tamanho = verificarCombo(locator, element);

            if (tamanho != 0) {
                switch (tipo) {
                    case "index":
                        if (!Objects.equals(tamanho, 1))
                            combo.selectByIndex(index);

                        else combo.selectByIndex(0);
                        break;

                    case "text":
                        combo.selectByVisibleText(texto);
                        break;

                    case "value":
                        combo.selectByValue(texto);
                        break;
                }
            }
        }
        log.debug("Combo selecionado... " + locator);
    }

    public void selecionarComboAleatorio(By locator) {
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        selecionarComboAleatorio(locator, 0);
    }

    /**
     * Seleciona um Combo aleatoriamente;
     *
     * @param locator Localizador do elemento
     */
    private void selecionarComboAleatorio(By locator, Integer interator) {
        log.debug("Seleciona um elemento do combo aleatoriamente...");

        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        setWait(() -> visibilityOfElementLocated(locator));

        WebElement element = checkElement(locator);
        if (element == null)
            element = getElement(locator);

        Select combo = new Select(element);
        List<WebElement> listaResultado = combo.getOptions();

        Integer tamanhoLista = listaResultado.size();

        Integer ran = TratarNumero.gerarNumeroAleatorio(1, tamanhoLista);

        selecionarCombo(locator, "index", null, ran - 1);

        String selecionado = getText(retornarElementoSelecionadoCombo(locator));

        selecionado = TratarString.espaco(selecionado);

        if (selecionado == null || Objects.equals(selecionado, "") && interator != 5)
            selecionarComboAleatorio(locator, ++interator);
    }

    /**
     * Escolhe quantas linhas serão apresentadas
     * ex. Quantos fornecedores irão aparecer na tela ListaFornecedor.mvc
     *
     * @param value '5', '10', '20' ou '50' linhas
     */
    public void selecionarComboLinhas(String value) {
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        log.debug("Seleciona combo com as opções de 5, 10, 20 e 50 linhas(não possui nome)...");
        setWait(() -> visibilityOfElementLocated(By.xpath("//select[@class='ui-corner-all']")));
        Select select = new Select(getElement(By.xpath("//select[@class='ui-corner-all']")));
        select.selectByValue(value);
    }

    /**
     * Troca para o Alert;
     *
     * @param tipo 'true' para OK, 'false' para Cancelar;
     */
    public void switchToAlert(boolean tipo) {
        log.debug("Troca para o Alert...");

        if (Wait.waitFor(2000, Wait::alertIsPresent)) {

            Alert alerta = webDriverInstance.switchTo().alert();
            log.info("Alert text: " + alerta.getText());
            if (tipo) {
                log.debug("Alert OK");
                try {
                    alerta.accept();
                } catch (Exception e) {
                    log.error(getClass().getSimpleName() + " - " + e);
                }
            } else {
                log.debug("Alert Cancel");
                try {
                    alerta.dismiss();
                } catch (Exception e) {
                    log.error(getClass().getSimpleName() + " - " + e);
                }
            }
        }
    }

    /**
     * Verifica se o elemento esta selecionado
     *
     * @param locator localizador do elemento
     * @return se o elemento estiver selecionado, retorna verdadeiro
     */
    protected Boolean isSelected(By locator) {
        log.debug("Verificando se o elemento esta selecionado");
        WebElement element = checkElement(locator);

        return (element != null) ?
                element.isSelected() : getElement(locator).isSelected();
    }

    /**
     * Pega o texto de um elemento pelo locator dele
     *
     * @param locator locatod do elemento
     * @return retona o texto do elemento
     */
    public String getText(By locator) {
        log.debug("Pega texto de um elemento");

        setWait(() -> visibilityOfElementLocated(locator));

        WebElement element = checkElement(locator);

        return (element != null) ?
                element.getText() : getElement(locator).getText();
    }

    /**
     * Pega o texto de um elemento pelo Element dele
     *
     * @param element Elemento
     * @return retorena o texto do elemento
     */
    protected String getText(WebElement element) {
        log.debug("Pega texto de um elemento");
        setWait(() -> visibilityOf(element));
        return element.getText();
    }

    /**
     * Pega atributo de um elemento
     *
     * @param locator  localizador do elemento
     * @param atribute nome do atributo
     * @return Retona o que tem dentro do atributo
     */
    public String getAttribute(By locator, String atribute) {
        log.debug("Pega texto de um elemento");

        setWait(() -> presenceOfAllElementsLocatedBy(locator));

        WebElement element = getElement(locator);

        return (element != null) ?
                element.getAttribute(atribute) : getElement(locator).getAttribute(atribute);
    }

    /**
     * Pega atributo de um elemento
     *
     * @param element  elemento
     * @param atribute nome do atributo
     * @return Retona o que tem dentro do atributo
     */
    protected String getAttribute(WebElement element, String atribute) {
        log.debug("Pega texto de um elemento");
        setWait(() -> visibilityOf(element));

        return element.getAttribute(atribute);
    }

    /**
     * Preenche um campo
     *
     * @param locator        localizador do campo
     * @param texto          Texto que ira preencher o campo
     * @param numeroElemento qual o elemento que vc deseja preencher entre os elementos igualis na tela
     * @param keyss          Comando para mandar de teclado
     */
    protected WebElement preencherCampo(By locator, String texto, Integer numeroElemento, Keys... keyss) {
        log.debug("Preenche qualquer campo...");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        setWait(() -> visibilityOfElementLocated(locator));

        List<WebElement> elementList = checkElements(locator);

        privatePreencherCampo(locator, texto, elementList.get(numeroElemento), keyss);
        return elementList.get(numeroElemento);
    }

    /**
     * Preenche um campo
     *
     * @param locator localizador do campo
     * @param texto   Texto que ira preencher o campo
     * @param keyss   Comando para mandar de teclado
     */
    public WebElement preencherCampo(By locator, String texto, Keys... keyss) {
        log.debug("Preenche qualquer campo...");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        setWait(() -> visibilityOfElementLocated(locator));

        WebElement element = checkElement(locator);

        privatePreencherCampo(locator, texto, element, keyss);

        return element;
    }

    /**
     * Apenas a operação de limpar e preencher
     *
     * @param element elemento
     * @param texto   texto a ser preenchido
     */
    public void preencherCampo(WebElement element, String texto) {
        try {
            element.clear();
            element.sendKeys(texto);
        }catch(InvalidElementStateException e){
            Actions performAct = new Actions(webDriverInstance);
            performAct.sendKeys(element, texto).build().perform();
        }catch(Exception e){
            js("arguments[0].value='"+ texto +"';", element);
        }
    }

    /**
     * Preenche um Campo Auto-Complete
     *
     * @param locator locator do campo
     * @param texto   Texto que ira preencher o campo auto complete
     */
    public void preencherCampoAutoComplete(By locator, String texto) {
        log.debug("Preenche qualquer campo Auto Complete....");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        clicar(locator);
        preencherCampo(locator, texto, Keys.DOWN, Keys.ENTER);
    }

    /**
     * Preenche um Campo Auto-Complete
     *
     * @param nomeCampo Nome do campo
     * @param count     Quantidade de vezes que será pressionado para baixo
     */
    public void selecionarCampoAutoComplete(String nomeCampo, Integer count) {
        log.debug("preenche campo de auto complete aleatório...");
        selecionarCampoAutoComplete(By.name(nomeCampo), count);
    }

    /**
     * Preenche um Campo Auto-Complete
     *
     * @param locator locator
     * @param count   Quantidade de vezes que será pressionado para baixo
     */
    public void selecionarCampoAutoComplete(By locator, Integer count) {
        log.debug("preenche campo de auto complete aleatório...");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        clicar(locator);

        for (int i = 0; count > i; i++)
            preencherCampo(locator, null, Keys.DOWN);

        preencherCampo(locator, null, Keys.ENTER);
    }

    /**
     * Verifica o texto preenchido de um elemento
     *
     * @param locator localizador do elemento do tipo By
     * @return Retirna o valor do elemento.
     */
    protected String verificaTextoPreenchido(By locator) {
        log.debug("Verifica o texto preenchido no label...");

        setWait(() -> visibilityOfElementLocated(locator));

        return getElement(locator).getAttribute("value");
    }

    /**
     * Volta para o Meu Mercado (redirecionando automaticamente para .asp ou .mvc)
     */
    public void voltarMeuMercado() {
        log.debug("Volta para o Meu Mercado...");
        acessarUrl(Data.get("url.meumercado"));
    }

    /**
     * Confirma o Alert do FF;
     */
    public void confirmarAlertFirefox() {
        log.debug("Confirma o Alert do FF...");
        try {
            webDriverInstance.switchTo().alert().accept();
            setWait();
        } catch (NoAlertPresentException noe) {
            log.debug("Mensagem de confirmacao do navegador nao exibida");
        }
    }

    /**
     * Seleciona o check box aleatorio por JS
     */
    public void selecionaRandomCheckboxJS() {
        log.debug("Seleciona o check box aleatorio por JS...");
        AssertPage assertPage = new AssertPage();
        assertPage.assertPage();

        By locator = By.cssSelector("input[class='multiSel'][type='checkbox']");
        if (existsElement(locator)) {
            if (getElement(locator).isSelected())
                jQuery("(\"input[class='multiSel'][type='checkbox']\").click()");
            jQuery("(\"input[class='multiSel'][type='checkbox']\").click()");
        }
    }

    //--------------------------------- PROTECTED METHODS ------------------------------------

    /**
     * @param texto String que será procurada no HTML
     * @return retorna se a String foi encontrada no HTML
     */
    public Boolean pageSourceContains(String texto) {
        log.debug("Verificando se texto está presente no HTML...");
        return getPageSource().contains(texto);
    }

    /**
     * @return retorna o pageSource completo da página
     */
    public String getPageSource() {
        log.debug("Baixando o page source da página...");
        return webDriverInstance.getPageSource();
    }

    /**
     * Retorna uma lista das opções do combo
     *
     * @param locator localizador do elemento
     * @return Retorna uma lista das opções do combo
     */
    protected List<WebElement> getComboOptions(By locator) {
        log.debug("Retorna as opções do combo");

        setWait(() -> existsElement(locator));

        WebElement element = checkElement(locator);

        if (element == null)
            element = getElement(locator);

        Select select = new Select(element);

        return select.getOptions();
    }

    /**
     * Retorna a lista de elementos dentro de um elemento
     *
     * @param locator     localizador do elemento
     * @param locatorList localizador para pegar a lista de elemento
     * @return retorna a lista de elemento pelo locator
     */
    protected List<WebElement> findElements(By locator, By locatorList) {
        log.debug("Retorna uma lista de elementos que esta dentro de um elemento");
        setWait(locator);
        WebElement element = checkElement(locator);

        return (element != null) ?
                element.findElements(locatorList) : getElement(locator).findElements(locatorList);
    }

    /**
     * Veririca se esta apacendo o elemento
     *
     * @param locator localizador do elemento
     * @return se tiver aparecendo o elemento, retorna verdadeiro
     */
    protected Boolean isDisplayed(By locator) {
        log.debug("Verirficando se esta mostrando na tela");
        setWait(locator);
        WebElement element = checkElement(locator);

        return (element != null) ?
                element.isSelected() : getElement(locator).isSelected();
    }

    /**
     * Verifica o valor que está selecionado no combo
     *
     * @param locator Localizador do elemento
     * @return Texto selecioanado no combo
     */
    protected WebElement retornarElementoSelecionadoCombo(By locator) {
        log.debug("Verifica o valor que está selecionado no combo...");
        setWait(locator);

        Select select = new Select(getElement(locator));

        return select.getFirstSelectedOption();
    }

    /**
     * Maximiza a tela
     */
    protected void maximizarTela() {
        log.debug("Maximizando a tela");
        if (!Data.get("browser").contains("headless"))
            webDriverInstance.manage().window().maximize();
    }

    /**
     * Deleta cookies
     */
    public void deletarCookies() {
        log.debug("Deletando cookies");
        if (!Data.get("browser").contains("headless"))
            webDriverInstance.manage().deleteAllCookies();
    }

    /**
     * Deleta cookies
     */
    public void deletarCookie(String... cookieName) {
        log.debug("Deletando cookie");
        if (!Data.get("browser").contains("headless")){
            for(String ck : cookieName){
                try{
                    webDriverInstance.manage().deleteCookie(webDriverInstance.manage().getCookieNamed(ck));
                }catch (Exception e){
                    log.debug("Cookie não encontrado para ser deletado!");
                }
            }
        }
    }

    /**
     * move o mouse para o locator indicado
     *
     * @param locator localizador do WebElement
     */
    protected void mouseMove(By locator) {
        Actions action = new Actions(webDriverInstance);
        action.moveToElement(getElement(locator)).build().perform();
    }

    /**
     * Sobrecarga.. quando existe mais de um frame na página, utilizar este método para selecionar algum especifico;
     *
     * @param index Qual o index do Frame que precisa.
     */
    protected void trocarFrame(int index) {
        log.debug("Realiza a troca de frame por nome de Frame...");
        setWait(By.tagName("iframe"));
        List<WebElement> frames = getElements(By.tagName("iframe"));
        webDriverInstance.switchTo().frame(frames.get(index));
    }

    /**
     * METODO PROTEGIDO
     * Verifica se o combo foi preenchido corretamente;
     *
     * @param locator localizador do elemento
     * @param tipo    'index', 'text', 'value';
     * @param texto   Se tiver, Texto que ira ser selecionado;
     * @param index   Se tiver, Index que ira selecionar;
     */
    protected void VerificarPreenchimentoCombo(By locator, String tipo, String texto, Integer index) {
        log.debug("Verificando se combo foi preenchido...");

        setWait(locator);
        Select combo = new Select(getElement(locator));
        String valorCombo = combo.getFirstSelectedOption().getText();
        if ("".equals(valorCombo) || valorCombo.isEmpty())
            selecionarCombo(locator, tipo, texto, index);
    }

    /**
     * @param locator Forma para localizar o combo
     * @return Retorna uma lista do conteúdo do combo
     */
    protected List<String> retornarListaTextosDoCombo(By locator) {
        log.debug("Verifica o valor que está selecionado no combo....");

        Select select = new Select(getElement(locator));

        List<WebElement> lista = select.getOptions();

        return lista.stream().map(WebElement::getText).collect(Collectors.toList());
    }
    //--------------------------------- PRIVATE METHODS ------------------------------------

    /**
     * Verifica se o elemento é clicavel
     *
     * @param element WebElement***
     * @return retorna se o elemento é ou não clicavel
     */
    protected Boolean checkClickable(WebElement element) {
        log.debug("Verificando se o elemento está clicavel...");
        int count = 0;
        while (elementToBeClickable(element).apply(webDriverInstance) == null && count++ != 10)
            setWait(500);

        return (elementToBeClickable(element).apply(webDriverInstance) != null);
    }

    /**
     * verifica se tem alert na tela.
     *
     * @return boolean
     */
    @NotNull
    protected Boolean checkAlert() {
        log.debug("verificando se tem alert na tela...");
        return (alertIsPresent().apply(webDriverInstance) != null);
    }

    /**
     * Mostra um elemento
     *
     * @param element CssSelector em tratamentos do elemento
     */
    protected void showElement(String element) {
        log.debug("Mostrando o elemento...");
        jQuery("(\"" + element + "\").show()");
        setWait(5000, () -> visibilityOfElementLocated(By.cssSelector(element)));//Esperando o elemento aparecer na tela para fazer uma açõa posteriror
    }

    /**
     * Verifica se a lista de elementos estavisivel na tela
     *
     * @param locator localizador do tipo By
     * @return Retorna a listasta verificada de elemento aparetir do localizador
     */
    @NotNull
    protected List<WebElement> checkElements(By locator) {
        log.debug("Verificando uma lista de elemento...");

        setWait(() -> visibilityOfAllElementsLocatedBy(locator));

        List<WebElement> listaElementos = getElements(locator);

        if (listaElementos.size() != 0) {
            if (visibilityOfElementLocated(locator)) {
                highlightElement(locator);
                return listaElementos;
            }
        }
        return listaElementos;
    }

    /**
     * Verifica se:
     * Elemento está dentro de um Alert
     * Elemento está visivel
     * Elemento está clicavel
     * Se entrar em todas as condições ele iria clicar no elementoretorna o elemento
     *
     * @param locator localizador do elemento
     * @return elemento que passar pelas condições
     */
    protected WebElement checkElement(By locator) {
        log.debug("verificando os elementos passador por locator ou por lista de elemento...");

        setWait(() -> presenceOfAllElementsLocatedBy(locator),
                () -> visibilityOfElementLocated(locator));

        WebElement element = getElement(locator);

        if (visibilityOf(element))
            if (Wait.elementToBeClickable(element)) {
                highlightElement(element);
                return element;
            }

        return element;
    }

    /**
     * Preenche um campo
     *
     * @param locator locator do elemento
     * @param texto   texto para ser preenchido
     * @param element elemento que deve ser preenchido
     * @param keyss   se precisar fazer um comando, mandar um elemento do tipo KEYS
     */
    private void privatePreencherCampo(By locator, String texto, WebElement element, Keys[] keyss) {
        int cont = 0;
        List<String> stringList = new ArrayList<>();

        if (texto != null) {
            while (cont != texto.length())
                stringList.add(texto.substring(cont, cont++ + 1));
        }

        if (element != null) {
            if (keyss != null) {
                if (texto != null)
                    preencherCampo(element, texto);

                for (Keys keys : keyss) {
                    Wait.waitFor(2000, () -> Wait.visibilityOfElementLocated(By.xpath("//a[@href=\"/explore/tags/"+texto+"/\"]")));
                    Wait.setWait(); // sem este Wait 'atropela' o auto-complete
                    try{
                        element.sendKeys(keys);
                    }catch(Exception e){
                        Actions performAct = new Actions(webDriverInstance);
                        performAct.sendKeys(element, keys).build().perform();
                    }
                }

            } else if (texto != null)
                preencherCampo(element, texto);

        } else
            log.error(String.format("Elemento não apresentado na tela... %s", locator));
    }


    /**
     * Clicando no elemento por Driver ou por JS se não der certo por Driver
     *
     * @param locator Locator do elemento
     * @param element Elemento retirado do Locator
     */
    private void clicar(By locator, WebElement element) {
        if (element != null)
            element.click();
        else checkElement(locator).click();
    }

    /**
     * Verifica se o Combo tem todos os parametros de dentro dele ja carregados;
     *
     * @param locator Localizador do elemento
     */
    private Integer verificarCombo(By locator, WebElement element) {
        log.debug("Verificando combo...");
        Integer tamanho = 0;

        setWait(locator);

        Select combo = new Select(element);

        while (!Objects.equals(tamanho, combo.getOptions().size())) {
            setWait();
            tamanho = combo.getOptions().size();
        }
        return tamanho;
    }

    /**
     * Faz highlight no elemento
     *
     * @param element elemento
     */
    private void highlightElement(WebElement element) {
        log.debug("Fazendo highlight no elemento...");
        js("arguments[0].style.border='3px solid " + Data.get("color") + "'", element);
    }


    /**
     * Método criado para preencher o famoso campo data . CHAMADO : https://issues.miisy.com/Misy/do/issue/show/671297
     *
     * @param data Data para ser preenchido
     * @param nome nome do objeto
     */
    public void preencherDataJavascriptByName(String nome, String data) {
        Wait.setWait(By.name(nome));
        JavascriptExecutor js = (JavascriptExecutor) webDriverInstance;
        js.executeScript("var x = document.getElementsByName('" + nome + "'); x[0].setAttribute('value', '" + data + "')");
    }


    //--------------------------------- END OF OTHER METHODS ------------------------------------
}