package br.com.sma.core.utilitario.entrada;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Gravar {
    // ------------------------------ FIELDS ------------------------------
    private static final Logger log = LoggerFactory.getLogger(Gravar.class.getSimpleName());
    private static final String TARGET_FILE = "target/";

    // -------------------------- OTHER METHODS --------------------------

    /**
     * Gava arquivos na target.
     *
     * @param content       Conteudo do arquivo que ira ser gravado.
     * @param caminhoGravar Caminho dentro da target.
     * @param nomeArquivo   Nome com a extesao que ira gravar.
     */
    public static void gravarNaTarget(String content, String caminhoGravar, String nomeArquivo) {
        gravarNaTarget(content, caminhoGravar, nomeArquivo, "ISO-8859-1");
    }

    /**
     * Gava arquivos na target.
     *
     * @param content       Conteudo do arquivo que ira ser gravado.
     * @param caminhoGravar Caminho dentro da target.
     * @param nomeArquivo   Nome com a extesao que ira gravar.
     * @param tipoEncode    tipo de Encode para gravar.
     */
    public static void gravarNaTarget(String content, String caminhoGravar, String nomeArquivo, String tipoEncode) {
        log.debug("gravando na /target\n" + caminhoGravar + nomeArquivo);

        gravar(content, TARGET_FILE + caminhoGravar, nomeArquivo, tipoEncode);
    }

    public static void gravar(String content, String nomeArquivo, String tipoEncode) {
        log.debug("gravando na /config... " + nomeArquivo);

        gravar(content, "src/main/resources/config", nomeArquivo, tipoEncode);
    }

    private static void gravar(String content, String path, String nomeArquivo, String tipoEncode) {
        log.debug("gravando... " + path);

        File file = new File(path);

        log.debug("Verificando se existe");
        if (!file.exists())
            file.mkdirs();

        try {
            Writer out = new OutputStreamWriter(new FileOutputStream(file + nomeArquivo), tipoEncode);
            log.debug("Iniciando gravacao em diretorio: " + file.getCanonicalPath());
            log.debug("Conteudo:\n" + content);

            log.debug("Gravando no arquivo");
            out.write(content);
            out.flush();

            log.debug("Fechando conexao");
            out.close();
        } catch (Exception e) {
            log.error("não foi possivel gravar na tagert" + e);
        }
    }
}
