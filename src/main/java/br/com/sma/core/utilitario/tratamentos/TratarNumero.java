package br.com.sma.core.utilitario.tratamentos;

import br.com.sma.core.utilitario.base.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class TratarNumero {
    // ------------------------------ FIELDS ------------------------------
    private static final Logger log = LoggerFactory.getLogger(Util.class.getSimpleName());

    // -------------------------- OTHER METHODS --------------------------

    /**
     * Gera numero aleatorio.
     *
     * @param numeroMinimo Numero minimo para gerar aleatoriamente.
     * @param numeroMaximo Numero maximo para gerar aleatoriamente.
     * @return Retorna um numero aleatorio entre o maximo e o minimo.
     */
    public static Integer gerarNumeroAleatorio(Integer numeroMinimo, Integer numeroMaximo) {
        log.debug("->Gera um numero aleatorio");
        int numero = 0;
        try {
            Random random = new Random();

            while (numero == 0) {
                numero = random.nextInt(numeroMaximo) + numeroMinimo;
                log.debug("Numero gerado = " + numero);
            }
        } catch (Exception e) {
            log.error("não foi possivel geral numero aleatorio... " + e);
        }
        return numero;
    }

    /**
     * Formata valor em milisegundos.
     *
     * @param ms Recebe um valor em milisegundos
     * @return Retorna um numero de fácil leitura em hh:mm:ss
     */
    public static String formatarTempo(long ms) {
        log.debug("Formatando valor em milisegundos...");
        return String.format( "%02d:%02d:%02d:%03d", ms / 3600000, ( ms / 60000 ) % 60 , ( ms / 1000 ) % 60, ms % 1000);
    }
}