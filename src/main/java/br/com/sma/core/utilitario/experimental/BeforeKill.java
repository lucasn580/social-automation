package br.com.sma.core.utilitario.experimental;

import br.com.sma.core.utilitario.driver.WebDriverRecicle;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Set;

class BeforeKill {

    private static void verificarThread() {
        System.out.println("oi");
        Thread.currentThread().setName("Thread-Kill");
        Thread.currentThread().setPriority(1);

        WebDriverRecicle.recicleWebDriver();
    }

    @NotNull
    private static Boolean verificarNomeThread() {
        Set<Thread> threads = Thread.getAllStackTraces().keySet();

        for (Thread thread : threads)
            if (Objects.equals(thread.getName(), "Thread-Kill"))
                return true;
        return false;
    }

    public void shutdownHook() {
        if (!verificarNomeThread())
            Runtime.getRuntime().addShutdownHook(new Thread(BeforeKill::verificarThread));
    }
}