package br.com.sma.core.auxiliares.acessos;

import br.com.sma.core.auxiliares.header.Header;
import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.driver.WebDriverFactory;
import br.com.sma.core.utilitario.properties.Data;

public class Logout extends Page {
    //--------------------------------- OTHER METHODS ------------------------------------

    /**
     * Desloga do sistema e Recicla o Driver;
     *
     * @param className Nome da class para o print;
     */
    public void deslogar(String className) {
        //--------------------------------- Pages ------------------------------------
        Header header = new Header();

        //--------------------------------- Deslogar ------------------------------------
        log.debug("Deslogando...");
        if (WebDriverFactory.verificarDriverAberto()) {
            if (!Data.get("browser").equals("firefox")) {
                if (!Wait.waitFor(1000, Wait::alertIsPresent)) {
                    verificarSePodeCapturarScreenshot(className);

                } else log.info("Não foi possível tirar Screenshot, pop-up na tela");
            } else {
                try {
                    verificarSePodeCapturarScreenshot(className);
                } catch (Exception e) {
                    log.error("Não foi possível tirar Screenshot, pop-up na tela");
                }
            }
            header.clicarLinkLogout();
        } else log.warn("Não foi possível tirar Screenshot, Driver não foi aberto");
    }

    private void verificarSePodeCapturarScreenshot(String className) {
        if (Data.get("UltimoErro") == null)
            capturarScreenshot(Data.get("nomeFluxo") + "_" + className);

        else {
            Data.remove("UltimoErro");
            capturarScreenshot(Data.get("nomeFluxo") + "_" + className + "_SegundaTentativa");
        }
    }
}