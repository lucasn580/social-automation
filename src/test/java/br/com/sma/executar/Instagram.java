package br.com.sma.executar;

import br.com.sma.medias.instagram.run.MeuAutomatizador;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {

        MeuAutomatizador.class
})

public class Instagram {
}