package br.com.sma.medias.facebook.tests;

import br.com.sma.core.paginas.FaceGrupo;
import br.com.sma.core.paginas.FaceInicial;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.support.base.BaseForFacebook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static br.com.sma.support.finalizar.Final.finalizar;
import static br.com.sma.support.iniciar.Inicio.iniciarFacebook;

public class TC1PublicarSeuProdutoEmGrupos extends BaseForFacebook {
    @Before
    public void setUp() {
        iniciarFacebook();
    }

    @Test
    public void publicarSeuProdutoEmGruposTest() {
        log.debug("Publicando produto nos grupos do Facebook");

        for(int i = 1; Data.get("palavra" + i) != null; i++){
            buscarGrupo(Data.get("palavra" + i));
            publicarProduto();
        }
    }

    // -------------------------- OTHER METHODS --------------------------
    private void buscarGrupo(String grupo) {
        log.debug("buscarGrupo");
        FaceInicial inicial = new FaceInicial();
        inicial.acessarGrupos();
        inicial.clicarGrupo(grupo);
    }

    private void publicarProduto(){
        log.debug("publicarProduto");
        FaceGrupo grupo = new FaceGrupo();
        grupo.preencherPublicacao();
        grupo.clicarBotaoPublicar();
    }

    @After
    public void tearDown() {
        finalizar(getClass().getSimpleName());
    }
}

