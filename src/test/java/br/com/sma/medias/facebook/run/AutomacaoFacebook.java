package br.com.sma.medias.facebook.run;

import br.com.sma.medias.facebook.tests.TC1PublicarSeuProdutoEmGrupos;
import br.com.sma.support.base.BaseForSuite;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        TC1PublicarSeuProdutoEmGrupos.class
})

public class AutomacaoFacebook extends BaseForSuite {
    @BeforeClass
    public static void setUp() {
        iniciar(AutomacaoFacebook.class.getSimpleName());
    }
}
