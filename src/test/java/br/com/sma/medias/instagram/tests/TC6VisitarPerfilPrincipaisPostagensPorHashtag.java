package br.com.sma.medias.instagram.tests;

import br.com.sma.core.paginas.InstaBusca;
import br.com.sma.core.paginas.InstaInicial;
import br.com.sma.core.paginas.InstaPublicacao;
import br.com.sma.core.utilitario.base.Wait;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.support.base.BaseForInstagram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static br.com.sma.support.finalizar.Final.finalizar;
import static br.com.sma.support.iniciar.Inicio.iniciar;

public class TC6VisitarPerfilPrincipaisPostagensPorHashtag extends BaseForInstagram {
    // -------------------------- OTHER METHODS --------------------------
    @Test
    public void seguirQuemCurtiuNovasPostagensTest() {
        log.debug("Contando tags");

        for(int i = 1; Data.get("tag" + i) != null; i++){
            buscarTag(Data.get("tag" + i));
            entrarNasPublicacoesDaTag();
        }
    }

    // -------------------------- PRIVATE METHODS --------------------------
    private void buscarTag(String valor) {
        log.debug("buscarTag");
        InstaInicial inicial = new InstaInicial();
        inicial.realizarBuscaTag(valor);
    }

    private void entrarNasPublicacoesDaTag(){
        log.debug("entrarNasPublicacoesDaTag");
        InstaBusca busca = new InstaBusca("Tag");
        InstaPublicacao post = new InstaPublicacao();

        for(int linha = 1; linha <= 3; linha++){
            for(int coluna = 1; coluna <= 3; coluna++){
                busca.clicarPublicacaoPrincipais(linha, coluna);
                Wait.setWait();
                post.visitarPerfisCurtidores(1);
                post.fecharPost();
            }
        }

    }

    // -------------------------- END OF OTHER METHODS --------------------------
    @Before
    public void setUp() {
        iniciar(Data.get("Email"));
    }

    @After
    public void tearDown() {
        finalizar(getClass().getSimpleName());
    }
}
