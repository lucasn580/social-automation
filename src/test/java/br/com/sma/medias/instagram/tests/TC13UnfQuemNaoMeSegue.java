package br.com.sma.medias.instagram.tests;

import br.com.sma.core.paginas.InstaInicial;
import br.com.sma.core.paginas.InstaPerfil;
import br.com.sma.core.utilitario.calendario.Date;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import br.com.sma.support.base.BaseForInstagram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static br.com.sma.support.finalizar.Final.finalizar;
import static br.com.sma.support.iniciar.Inicio.iniciar;

public class TC13UnfQuemNaoMeSegue extends BaseForInstagram {
    // -------------------------- FIELDS --------------------------
    private List<String> seguidores = new ArrayList<>();
    private List<String> naoSeguidores = new ArrayList<>();

    // -------------------------- OTHER METHODS --------------------------
    @Test
    public void unfollowPerfisQueNaoMeSeguem() {
        log.debug("Unfollow nos perfis que não me seguem");

        log.debug("acessarMeuPerfil");
        acessarMeuPerfil();

        log.debug("checarMeusSeguidores");
        checarMeusSeguidores();

        log.debug("checarQuemNaoMeSegueDeVolta");
        checarQuemNaoMeSegueDeVolta();

        log.debug("unfollowQuemNaoMeSegueDeVolta");
        unfollowQuemNaoMeSegueDeVolta();
    }

    // -------------------------- PRIVATE METHODS --------------------------
    private void acessarMeuPerfil() {
        InstaInicial inicial = new InstaInicial();
        inicial.acessarMeuPerfil();
    }

    private void checarMeusSeguidores(){
        InstaPerfil perfil = new InstaPerfil();

        int contagem = perfil.contarSeguidores();
        log.info(TratarString.colored("Número atual de seguidores: " + contagem, TratarString.Cores.VERDE, null, TratarString.Formatacao.NEGRITO));

        perfil.abrirSeguidores();

        seguidores = perfil.capturarSeguidores(contagem);

        perfil.fecharSeguidores();
    }

    private void checarQuemNaoMeSegueDeVolta(){
        int perfisSeguidos = Data.countProperty("PerfilSeguido");

        for(int i = 1; i <= perfisSeguidos; i++){

            // Verificando se já se passaram 2 dias de tempo limite para o usuário seguir de volta
            // Assim será feito 'unfollow'
            Date data = new Date();
            long diasPassados = data.diferencaEntreDatas(Data.get("DataPerfil"+i), data.getDate());

            if(!seguidores.contains(Data.get("PerfilSeguido"+i)) && diasPassados > 2){
                naoSeguidores.add(Data.get("PerfilSeguido"+i));
            }
        }
    }

    private void unfollowQuemNaoMeSegueDeVolta(){
        InstaPerfil perfil = new InstaPerfil();
        for (String nome : naoSeguidores){
            perfil.visitarPerfil(nome);
            perfil.deixarDeSeguir();
        }
    }

    // -------------------------- END OF OTHER METHODS --------------------------
    @Before
    public void setUp() {
        iniciar(Data.get("Email"));
    }

    @After
    public void tearDown() {
        finalizar(getClass().getSimpleName());
    }
}
