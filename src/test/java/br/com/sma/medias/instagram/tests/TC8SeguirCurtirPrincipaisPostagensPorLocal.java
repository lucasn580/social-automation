package br.com.sma.medias.instagram.tests;

import br.com.sma.core.paginas.InstaBusca;
import br.com.sma.core.paginas.InstaInicial;
import br.com.sma.core.paginas.InstaPublicacao;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.support.base.BaseForInstagram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static br.com.sma.support.finalizar.Final.finalizar;
import static br.com.sma.support.iniciar.Inicio.iniciar;

public class TC8SeguirCurtirPrincipaisPostagensPorLocal extends BaseForInstagram {
    // -------------------------- OTHER METHODS --------------------------
    @Test
    public void seguirCurtirPrincipaisPostagensTest() {
        log.debug("Contando locais");

        for(int i = 1; Data.get("local" + i) != null; i++){
            acessarLocal(Data.get("local" + i));
            entrarNasPublicacoesDoLocal();
        }
    }

    // -------------------------- PRIVATE METHODS --------------------------
    private void acessarLocal(String local){
        log.debug("acessarLocal");
        InstaInicial inicial = new InstaInicial();
        inicial.realizarBuscaLocal(local);
    }

    private void entrarNasPublicacoesDoLocal(){
        log.debug("entrarNasPublicacoesDoLocal");
        InstaBusca busca = new InstaBusca("Local");
        InstaPublicacao post = new InstaPublicacao();

        for(int linha = 1; linha <= 3; linha++){
            for(int coluna = 1; coluna <= 3; coluna++){
                busca.clicarPublicacaoPrincipais(linha, coluna);
                post.like();
                post.follow();
                post.fecharPost();
            }
        }

    }

    // -------------------------- END OF OTHER METHODS --------------------------
    @Before
    public void setUp() {
        iniciar(Data.get("Email"));
    }

    @After
    public void tearDown() {
        finalizar(getClass().getSimpleName());
    }
}
