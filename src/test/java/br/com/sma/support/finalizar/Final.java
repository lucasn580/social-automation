package br.com.sma.support.finalizar;

import br.com.sma.core.auxiliares.acessos.Logout;
import br.com.sma.support.base.BaseForTests;

public class Final extends BaseForTests {
    // -------------------------- OTHERS METHODS --------------------------

    /**
     * Faz o Logoff do usuario e tira print da tela;
     *
     * @param className Nome da classe para se o nome do Print;
     */
    public static void finalizar(String className) {
        Logout logout = new Logout();
        logout.deslogar(className);
    }
    // -------------------------- END OF OTHERS METHODS --------------------------
}
