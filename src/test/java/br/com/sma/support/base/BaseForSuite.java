package br.com.sma.support.base;

import br.com.sma.core.utilitario.driver.WebDriverRecicle;
import br.com.sma.core.utilitario.driver.WebDriverRemoveFiles;
import br.com.sma.core.utilitario.entrada.Gravar;
import br.com.sma.core.utilitario.properties.Data;
import br.com.sma.core.utilitario.saida.Ler;
import br.com.sma.core.utilitario.tratamentos.TratarString;
import org.junit.AfterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class BaseForSuite {
    // -------------------------- FIELDS --------------------------
    private static final Logger log = LoggerFactory.getLogger(BaseForSuite.class.getSimpleName());

    // -------------------------- PRIVATE METHODS --------------------------
    protected static void iniciar(String nomeFluxo) {
        log.debug("Limpando as propriedades do outro fluxo");
        Data.clear();

        log.debug("Mostrando branch");
        showBranch();

        log.debug("pegando nome do fluxo");
        Data.set("nomeFluxo", nomeFluxo);

        log.debug("Selecionando Plataforma");
        selecionarPlataforma();

        log.debug("Removendo arquivos Temporarios");
        WebDriverRemoveFiles.removeFiles();
    }

    private static void finalizarSuite() {
        WebDriverRecicle.recicleWebDriver();
        WebDriverRemoveFiles.removeFiles();
        gravarProp();
    }

    private static void gravarProp() {
        String conteudo = Ler.ler("target/prop/fluxo.properties") + "\n" + Data.getProperties().replaceAll("listing properties", Data.get("nomeFluxo"));

        Gravar.gravarNaTarget(conteudo, "/prop", "/fluxo.properties", "UTF-8");
    }

    private static void selecionarPlataforma() {
        if (Objects.equals(Data.get("Plataforma"), "Instagram"))
            Data.getResourceProperties("config/instagram.properties");

        else Data.getResourceProperties("config/facebook.properties");
    }

    private static void showBranch() {
        if (Data.get("Branch") == null) {

            try {
                Process p = Runtime.getRuntime().exec("git name-rev --name-only HEAD");

                Data.p.setProperty("Branch", new BufferedReader(new InputStreamReader(p.getInputStream())).readLine());

                String mensagem = "\n\n" +
                        "------------------------------------------------------------------------------------------------------------------------------" +
                        "\n\n" +
                        "                                   Branch: " + TratarString.colored(TratarString.tudoU(Data.get("Branch")), TratarString.Cores.VERMELHO,
                                                                                                                                     TratarString.Fundo.AZUL,
                                                                                                                                     TratarString.Formatacao.PISCANDO) + "\n" +
                        "\n" +
                        "------------------------------------------------------------------------------------------------------------------------------" +
                        "\n";

                log.info(mensagem);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // -------------------------- END OF OTHER METHODS --------------------------
    @AfterClass
    public static void tearsDown() {
        finalizarSuite();
    }
}