package br.com.sma.support.base;

import br.com.sma.core.utilitario.base.Page;
import br.com.sma.core.utilitario.driver.WebDriverRecicle;
import br.com.sma.core.utilitario.experimental.junit.ListenerAwareJUnitRunner;
import br.com.sma.core.utilitario.properties.Data;
import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(ListenerAwareJUnitRunner.class)
public class BaseForTests {
    // -------------------------- Filds --------------------------
    protected static final Logger log = LoggerFactory.getLogger(BaseForTests.class.getSimpleName());
    protected static Page page;

    // -------------------------- OTHERS METHODS --------------------------
    @BeforeClass
    public static void selecionarPlataforma() {
        if (Data.get("Plataforma").equals("Instagram") || Data.get("Plataforma").equals("Facebook"))
            Data.getResourceProperties("config/configuracao.properties");
    }

    // -------------------------- END OF OTHERS METHODS --------------------------
    @Before
    public void setUpBase() {
        page = new Page();
    }

    @After
    public void tearDownBase() {
        WebDriverRecicle.recicleWebDriver();
    }
}